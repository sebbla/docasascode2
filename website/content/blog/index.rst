#. Select **Advanced Settings**.
#. Find the **Course Advertised Start Date** policy key.
#. Enter the value you want to display.

* Who is teaching the course?
* What university or college is the course affiliated with?
* What topics and concepts are covered in your course?
* Why should a learner enroll in your course?

#. Review your entry to verify that the key is accurate and that it is
   surrounded by quotation marks. If there is a list of keys, they must be
   comma separated.

   * In this example, the key for the Annotation Problem tool is the only
     value in the list.

   * In this example, the key for the Annotation Problem tool is added at
     the beginning of a list of other keys.

#. Select **Save Changes**.
